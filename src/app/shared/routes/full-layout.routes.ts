import { Routes, RouterModule } from '@angular/router';

//Route for content layout with sidebar, navbar and footer.

export const Full_ROUTES: Routes = [
  {
    path: 'signature',
    loadChildren: () => import('../../components/ui-components.module').then(m => m.UIComponentsModule)
  }
];
