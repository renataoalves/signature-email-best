import { Component, ViewChild, ElementRef } from '@angular/core';
import { NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { saveAs } from '@progress/kendo-file-saver';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
})
export class TabsComponent {
  t: any;
  currentJustify = 'start';
  currentOrientation = 'horizontal';

  MANY_ITEMS = 'MANY_ITEMS';

  SHAPE_NORMAL = 0;
  SHAPE_CUSTOM = 1;
  SHAPE_CIRCLE = 50;

  fontFamily = [
    { name: 'Arial' },
    { name: 'Tahoma' },
    { name: 'Times New Roman' },
    { name: 'Trebuchet MS' },
    { name: 'Verdana' }
  ];

  imagesSize = [
    { id: "50px", name: 'Pequena (50x50)' },
    { id: "75px", name: 'Média (75x75)' },
    { id: "100px", name: 'Grande (100x100)' }
  ];

  isCollapsedHowToInsertImage = true;

  socialNetworks = [{
    icon: "/assets/social-network/32/linkedin.png",
    icon_email: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAAB8AAAAfAEVD+3kAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAH5QTFRFAISxAYSxBYazB4ezDIq1KJe9Lpq/MpzAOJ/CPaHEQKPFSafHUqzKVq3LX7LOZLTQaLbRbrnTcLrTdLzUiMbbisfbkMndlMvemM3foNHiqtblsNnntNvouN3pud3qvN/r3O704PD26PT48fj78vn79Pr89/v9+v39/f7+////AzDh/wAAAFtJREFUGFeNyTUWgEAUQ9Hg7jC469//BuEAxUwFr8sN8CM1tkRIaJYFsJdW2MD7K9K7nbFDOubVPkQPhDShpIOudA5Ww9wo46ABeio4qIH2N2gshsd8IGDuDR+dSCIJaEbM/RIAAAAASUVORK5CYII=',
    tooltip: "LinkedIn",
    added: false,
    placeholder: "https://www.linkedin.com/in/your-id"
  }, {
    icon: "/assets/social-network/32/facebook.png",
    icon_email: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAAWQAAAFkBqp2phgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAEISURBVDiNY3RK33yCgeG/OQNZgPEkE/maGRgYGP6bsxCrVE6Ch8HJVJqBk52F4fjllwwXbr5hYGBgYCDKgKYMUwZrAwk4/9uPP8QboKcqDNf84fMvhuv33zM8fPEZLk/QAHlJHji7oOcow+OXX1DkmQgZwMbKDGe///wTQx6nC+yNpRgsdMQY5CV54WL5kboMf/78Y9hx/DHDxVtv8RugJsfP4GYpiyLmZCrNwMDAwHDw3HPCLnj78QfD3SefGAR42RiE+TkYGBgYGO4/+8zw799/hscvEOHA6JS+6T8uQxgYGBiCnZUYskK1GRgYGBj8i3YwfPn2G0WeYCASAsPDAMaT5GtnPAkAC5ZLEBbqG4cAAAAASUVORK5CYII=',
    tooltip: "Facebook",
    added: false,
    placeholder: "https://www.facebook.com/your-id"
  }, {
    icon: "/assets/social-network/32/instagram.png",
    icon_email: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAAdgAAAHYBTnsmCAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAK7SURBVDiNVZPbi1ZlFMZ/a73vfN9+vwbGhgh1BrsYipgIsotQYsQaCZHCogksAhESTEiIEDoQIUH/gGB1MdCVIZ2wDJ2LUYyaDtBFB4UwoRlwLlRqxu+49373u7rYU+rFYj3PgvXwLB6WANjM1O5YtV6Jlm0uq2w0poyYAjFllFXd12ZFrLJuaeFCmbL3tv7w3Gmxg48/QTudsb5JrG4uxhQoU8atszKFNZ4RU7CYmjs9DXmbICJm+IlJ3M5naW7chKxbB+pI/Rxiwgzyy9e4fvxnOhdWMEQMeUvszR1tujbM+INw4Ah24iPipUWq0hE7FTFlpDBCJQE/eS8jLz7G0muf0bm4Qkyh5wnaxBJM74a5E8j857jCYSkgVYakgKWMxqNbcRtHuPHF99z1/MMM3jmNIQ1PEMUUxidg4WsIgg55hvbsp7FtB2ZQnFugP/8T8RcoVyN3TD2A0xwzQQkqBIGsBeQQFPa+jGxYT3X4IMXhV/HjdxOmHyEu/Ajtf5AhxesArzlKECUoqILm0FLYMg0fH8Vd/wt3dZE4+yGN7VtwrsC7Amk4vOY4zfG0FCyBAE2pHdyCfS8HKQDwmmNaIiI4yTGtT6iXMGg1IAC/noUXDsGmMbhnDP/SAez8PF76OFeCJbzmdREEkkDRhuFQi52fhal98Mb7YMC3c8jxWZwK0hTodfE6wBA8QSsSjs4SbLgPln+HVMLZD+CrY9BL0DVEEl4dNjmBLS7WKdQCskqSUZbPwEOvgygs/wmrbehFWOnAAMhG0bH74ek9lO8ewWsOSCF2ceYc3bSdboJsEsZ2wfA4ZHcCCgBVBTf+hitLcOoTqt/++O9XvhG7PPMkffuSrkltN/1v+yZO0Ludx6qZqqq1S2Xi01M05RmCLBBkmSCDOhmB1lqsrTUepE/QKwT9zkv+VHby5Ny/2vlHdhzfwHEAAAAASUVORK5CYII=',
    tooltip: "Instagram",
    added: false,
    placeholder: "https://www.instagram.com/your-id"
  }, {
    icon: "/assets/social-network/32/google+.png",
    icon_email: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAAbwAAAG8B8aLcQwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAEESURBVDiNxVO9SgNBGJxZLtWRLk+RyiYnHNiYWOUNLC1tQ0BIqyAGLNNKIG9xpsoheo0vEXyAHIc27lhcNlziXgQjONXHMjPf336UhENgDlL/hUHggrwbxSSHhCKAAYBXS143k+d0nwEloTiLriDeeCpavn802q00zesMTN6N4rX4E9QDDTqwOAJwD7DvExe9483kA5JDAAbUNEyyiwpvsLf5jUHZM0hOfBkcwscXVt9dHOwS/dCbM3FiFxuBGQBIuqxmWxPuAMBxfODqtBMbcoFyiDOSEytTwOqc1AiArHTSnGdPXoMf1mhBjcIku62twN3C9kcqy5Y0rsv8zeC3+P9j+gI/K3Rhzmrt/QAAAABJRU5ErkJggg==',
    tooltip: "Google+",
    added: false,
    placeholder: "https://plus.google.com/+teu+id"
  }, {
    icon: "/assets/social-network/32/twitter.png",
    icon_email: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAAbwAAAG8B8aLcQwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAGUSURBVDiNpZO/a1RBFIW/O2/WhbxEAyJaGcGAiI2FIGhhCsFfhdXukiJVxNJOCzux00YL/4AtJOpCiCI2C6aOhGAhiIpgYxuE5D13N/PesdgX2Q1vddEDA8PlfmfOHWZMEv8jP06TtZIjEXZNopJrp63Gga8A9nRzv/FGVb+VPAgW31aN3l640tq+INkKMF2UukhNnB3P0LzzW9vnBLcipW9tKTm810C5NQdggCpmNxEWya66IH0vWs9HEev+eXrDWp2jAPascwxjZsRk30I9bpok/IukDVwcmhs+CTaA+fKLseuhNvHK72slp4EKoD5X5IET9FepMtx7gCJB+gU0O6q5RJuhHh8EcP24+aLB5/F5W93dOYAwNbkmrA10x6B7mbK7Qwa6QlfkH4HqX8+GR2pM/U7rdjdZffKJMzuD8foP9Hqw9P5gyQPYyo9p34tOCVsAu1zKisfB4juqxUOv1SpL6dnc5feASyXcT7ANxMPQmHhZblz8RlvuzPgsO5mjWcP1HHq3cyj+oDnCyJEGDf5VvwCmpZgonjpXdwAAAABJRU5ErkJggg==',
    tooltip: "Twitter",
    added: false,
    placeholder: "https://www.twitter.com/your-id"
  }, {
    icon: "/assets/social-network/32/youtube.png",
    icon_email: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAAWQAAAFkBqp2phgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAADHSURBVDiNxdMtTgNREMDx33tBApKtwqwlmCYV1SRwj56hx+kVeosuSSXBrqnqWkBAzWC6CeKVLF3BJP+MmK/MV4oIYySPisYFSOkWd7guAG8FXkXsBI/BVxB/5DN4EKzPCO5ZZ0yKzdX1kBFMMqqiablks2E6/S1BdToBzOdst6xWVEW3m7FrzBndSXPTMJuxWNAV3TpBU5xwXQ/ZwnPGvli9bYe0sO8P6XDGDRyCpxQRpFTjHlc/uDxqeMfHUfe8iGjTv3/jNy6Ep9IgbWhqAAAAAElFTkSuQmCC',
    tooltip: "YouTube",
    added: false,
    placeholder: "https://www.youtube.com/your-id"
  }, {
    icon: "/assets/social-network/32/github.png",
    icon_email: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAAdgAAAHYBTnsmCAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAHpSURBVDiNhZO/axRREMc/89Y7kEjYCGl3eXuroBbBRhQRESshgrWN+AuDjWDp/xCSs0mhgl0QrOwstDkMCKZII1y43XvnYSqLlTQJy97YvA1rcppp3vvOfOc7zJf3hKMRWGuvishNIPK5kap+Gg6HG0DVJEsTxHF8yxizDJybIoyIfJ9MJs+dcx8PptUXa+0LEXkDzE9r9jEvInfDMNwviuLLgYC19iHQFZEdEfkMdJriPvZV9YOInBaRO2EYuqIotiRN0/mqqgbArKquOeeeJklyVlUXjDG5qoqqJkEQbA4Gg8xa+xa4BxStVuvMiaqqloBZv+MMQJ7n28B2Y/q3xv2kP8OyLJ8YYLGuqOraf/afxrltgNSDXefc1+MEnHM9YM/D1AAzHgigxwl4Tm3wjAF2PDgVx/HF47o7nc5loAWgqj8N0KuLxphumqaz/2qOomhuMpmsNPg9A7z2+CXQqqoqs9YuR1F0viZaaxeSJFkNgiADLtV5EXllhsNhD3gP3AeWgC1gUVWLxvA9VX0GzDVy77Is2zBe6RGQiciKiDyI4/jCeDyuvaEsyx+Httlst9uPAQxAnue/gyC4rqq/VHU0Go1Kjj5lvHHr7Xb7Rr/f351qlLX2mrW2y98/NUiSZDWO4yuH+X8A/3CwGz4vFw4AAAAASUVORK5CYII=',
    tooltip: "Github",
    added: false,
    placeholder: "https://github.com/your-id"
  }, {
    icon: "/assets/social-network/32/bitbucket.png",
    icon_email: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAAHdElNRQfkBRMNHDQEVr6OAAABCUlEQVQoz3XQTSuEcRQF8N+MSYMF8jJkKYq1l7Jhp2zMyk5Z2PMJpvkgSrMRsbVEtnbCNOSlqBlqpinvGXosnocJuXd1zj33nP//xkCftCkDujQhcCtv37oysORNRUbahH6DJszKqnqxGArOBS4sGNahQVy7IfMuBK5Cwa7gn94ljjP/1TEJnHwTTzYkzElGOB8KjiMYmFbzIWen7gCdUeKpRjXPki4jpj10KLuTQp8mOe9adIOi6lf2WrSxZ8yovQjl6q9Nyij9+GBRVjNhBCkxg3qMaNWo4siNZZ2uiUUOD15tO1AS02vcjKQ2j/WQwz9XLISDeCTY/HPFld9E2paCe0/yVk1+0Z9tvGlCcO+oXAAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAyMC0wNS0xOVQxMzoyODo1MiswMDowMNzKRRUAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMjAtMDUtMTlUMTM6Mjg6NTIrMDA6MDCtl/2pAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAABJRU5ErkJggg==',
    tooltip: "Bitbucket",
    added: false,
    placeholder: "https://www.bitbucket.org/your-id"
  }];
  socialNetworksSelected = [];

  placeholder = {
    name: "Renatão Alves",
    currentPosition: "Scrum Master",
    departament: "Development",
    company: "The Dreams",
    email: "renatao@thebest.com",
    website: "best-email-signature-generator.herokuapp.com",
    phone: "81 9999 2424",
    cellphone: "81 9 2323 1515",
    skype: "live:.cid.11f1935d3a67cf24",
    address: "Your address, n 69. Apto 24. Recife",
    image: {
      url: "https://media-exp1.licdn.com/dms/image/C4E03AQHkbQIuIbQIBA/profile-displayphoto-shrink_100_100/0?e=1594857600&v=beta&t=stTW5wpi01qm3YhvXlav24ykMvgsYD7JaRSnD34K7qk",
      link: "",
      shape: "",
      size: this.imagesSize[1].id,
      radius: 0
    },
    color: {
      title: "#CECECE",
      text: "#CECECE",
      theme: "#CECECE",
      link: "#CECECE"
    }
  }

  isDefaultSignature: boolean = true;
  signature = {
    name: "",
    currentPosition: "",
    departament: "",
    company: "",
    email: "",
    website: "",
    phone: "",
    cellphone: "",
    isWhatsapp: false,
    isTelegram: false,
    skype: "",
    address: "",
    image: {
      url: "",
      link: "",
      shape: "",
      size: this.imagesSize[1].id,
      radius: 0
    },
    socialNetworksSelected: this.socialNetworksSelected,
    font: {
      size: 13,
      family: this.fontFamily[0].name
    },
    color: {
      title: "#CECECE",
      text: "#CECECE",
      theme: "#CECECE",
      link: "#CECECE"
    }
  }

  contentFile: string;

  @ViewChild("signatureModel", { static: false }) element: ElementRef;

  constructor(public toastr: ToastrService) { }

  addItem(socialNetwork) {
    socialNetwork.added = true;

    let social = Object.assign({}, socialNetwork);
    this.socialNetworksSelected.push(social);
  }

  removeItem(socialNetwork, index) {
    this.socialNetworksSelected.splice(index, 1);

    let item = this.socialNetworks.filter(item => item.icon == socialNetwork.icon)[0];
    let selecteds = this.socialNetworksSelected.filter(item => item.icon == socialNetwork.icon).length;
    item.added = selecteds > 0;
  }

  lengthSocialNetwork() {
    return this.socialNetworksSelected.filter(item => item.url).length;
  }

  notify(payload: string) {
    console.info(`'${payload}' has been copied to clipboard`);
  }

  copy(event: MouseEvent, from: string): void {

    if (event)
      event.preventDefault();

    let listener = (e: ClipboardEvent) => {
      let clipboard = e.clipboardData || window["clipboardData"];

      var string = this.element.nativeElement.outerHTML.toString();
      string = string.replace("class=\"ng-star-inserted\"", "");
      string = string.replace("ng-reflect-ng-style=\"[object Object]\"", "");
      string = string.replace(/<!--.*?-->/sg, "");
      string = string.replace(/_ngcontent.*?-c4=""/sg, "");
      string = string.replace("  ", "");

      clipboard.setData("text", string);

      this.contentFile = string;

      if (from == 'copy')
        this.toastr.info('Texto copiado para área de transferência. Basta colar (ctrl + v / command + v) onde desejar.', 'Info!');

      e.preventDefault();
    };

    document.addEventListener("copy", listener, false)
    document.execCommand("copy");
    document.removeEventListener("copy", listener, false);
  }

  download(event: MouseEvent, from: string) {
    this.copy(event, from);

    let blob = new Blob([this.contentFile], { type: "text/plain;charset=utf-8" });
    saveAs(blob, "your-best-signature.html");

    this.toastr.success('Download realizado com sucesso.', 'Sucesso!');
  }

  new() {
    this.isDefaultSignature = false;
  }

  public beforeChange($event: NgbTabChangeEvent) {
    if ($event.nextId === 'bar') {
      $event.preventDefault();
    }
  }

}
